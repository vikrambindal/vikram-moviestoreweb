package demo.vikram.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import demo.vikram.dao.MovieDAO;
import demo.vikram.domain.Movie;
import demo.vikram.exception.RecordNotFoundException;
import demo.vikram.service.MovieService;

@Service("movieService")
public class MovieServiceImpl implements MovieService {

	private static final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);
	
	@Autowired
	private MovieDAO movieDAO;

	@Override
	public Movie findMovieById(Integer movieId) throws RecordNotFoundException {
		
		logger.info("findMovieById (movieId={})", new Object[]{movieId});
		
		return movieDAO.findMovieById(movieId);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public List<Movie> findAllMovies() {
		
		logger.info("findAllMovies" );
		
		return movieDAO.findAllMovies();
	}

	@Override
	public void saveOrUpdateMovie(Movie movie) {
		
		logger.info("saveOrUpdateMovie");
		
		movieDAO.saveOrUpdateMovie(movie);
	}
}
