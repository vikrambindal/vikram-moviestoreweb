package demo.vikram.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import demo.vikram.domain.Movie;
import demo.vikram.exception.RecordNotFoundException;

public interface MovieService {

	public Movie findMovieById(Integer movieId) throws RecordNotFoundException;
	
	public List<Movie> findAllMovies();
	
	@Transactional
	public void saveOrUpdateMovie(Movie movie);
}
