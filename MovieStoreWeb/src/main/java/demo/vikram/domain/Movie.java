package demo.vikram.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

@Entity
@XmlRootElement
@Table(name="movie")
public class Movie implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String DBCOL_MOVIE_ID = "id";
	public static final String DBCOL_MOVIE_NAME = "name";
	public static final String DBCOL_MOVIE_RATING = "rating";
	public static final String DBCOL_MOVIE_RELEASE = "yearOfRelease";
	public static final String DBCOL_MOVIE_GENRE = "genre";
	

	@Id
	@GenericGenerator(name = "unique_id", strategy = "native")
	@GeneratedValue(generator = "unique_id")
	@Column(unique = true, nullable = false, length = 35)
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="rating")
	private int rating;
	
	@Temporal(TemporalType.DATE)
	@Column(name="yearOfRelease")
	private Date yearOfRelease;
	
	@Column(name="genre")
	private String genre;

	@Transient
	private String release;
	
	public Movie() {
	}

	public Movie(String name, int rating, Date yearOfRelease, String genre) {
		super();
		this.name = name;
		this.rating = rating;
		this.yearOfRelease = yearOfRelease;
		this.genre = genre;
	}

	
	
	
	public String getRelease() {
		return release;
	}

	public void setRelease(String release) {
		this.release = release;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Date getYearOfRelease() {
		return yearOfRelease;
	}

	public void setYearOfRelease(Date yearOfRelease) {
		this.yearOfRelease = yearOfRelease;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	@Override
	public String toString() {
		return "Movie [id=" + id + ", name=" + name + ", rating=" + rating
				+ ", yearOfRelease=" + yearOfRelease + ", genre=" + genre + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((genre == null) ? 0 : genre.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + rating;
		result = prime * result
				+ ((yearOfRelease == null) ? 0 : yearOfRelease.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (genre == null) {
			if (other.genre != null)
				return false;
		} else if (!genre.equals(other.genre))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (rating != other.rating)
			return false;
		if (yearOfRelease == null) {
			if (other.yearOfRelease != null)
				return false;
		} else if (!yearOfRelease.equals(other.yearOfRelease))
			return false;
		return true;
	}
}
