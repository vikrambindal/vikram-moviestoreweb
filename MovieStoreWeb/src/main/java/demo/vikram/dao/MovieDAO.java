package demo.vikram.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import demo.vikram.domain.Movie;
import demo.vikram.exception.RecordNotFoundException;

public interface MovieDAO {

	public Movie findMovieById(Integer movieId) throws RecordNotFoundException;
	
	public List<Movie> findAllMovies();
	
	public void saveOrUpdateMovie(Movie movie);
}
