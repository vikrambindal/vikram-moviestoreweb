package demo.vikram.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import demo.vikram.dao.MovieDAO;
import demo.vikram.domain.Movie;
import demo.vikram.exception.RecordNotFoundException;

@Repository("movieDAO")
public class MovieDAOImpl extends AbstractDAOImpl implements MovieDAO {

	private static final Logger logger = LoggerFactory.getLogger(MovieDAOImpl.class);
	
	@Override
	public Movie findMovieById(Integer movieId) throws RecordNotFoundException {
		
		logger.debug("findMovieById (movieId={0})", movieId);
		
		Criteria movieCriteria = getCriteria(Movie.class);
		movieCriteria.add(Restrictions.eq(Movie.DBCOL_MOVIE_ID, movieId));
		Movie movie = (Movie) movieCriteria.uniqueResult();
		if(movie == null){
			throw new RecordNotFoundException("No movie found Id : " + movieId);
		}
		return movie;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Movie> findAllMovies() {
		
		logger.debug("findAllMovies");
		
		Criteria movieCriteria = getCriteria(Movie.class);
		return movieCriteria.list();
	}

	@Override
	public void saveOrUpdateMovie(Movie movie) {
		
		logger.debug("saveOrUpdate");
		
		saveOrUpdate(movie);
	}
}
