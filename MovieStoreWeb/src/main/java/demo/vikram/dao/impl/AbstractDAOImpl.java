package demo.vikram.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import demo.vikram.dao.AbstractDAO;

@Repository("abstractDAO")
public class AbstractDAOImpl implements AbstractDAO {

	//@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
		System.out.println("Session factory set");
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void saveOrUpdate(Object o){
		getSession().save(o);
		getSession().flush();
	}

	@Override
	public Criteria getCriteria(Class<?> clazz) {
		return getSession().createCriteria(clazz);
	}

	@Override
	public Criteria getCriteria(Class<?> clazz, String alias) {
		return getSession().createCriteria(clazz, alias);
	}

}
