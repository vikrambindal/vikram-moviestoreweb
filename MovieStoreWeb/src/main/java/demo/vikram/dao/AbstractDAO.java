package demo.vikram.dao;

import org.hibernate.Criteria;

public interface AbstractDAO {

	public Criteria getCriteria(Class<?> clazz);
	
	public Criteria getCriteria(Class<?> clazz, String alias);

	void saveOrUpdate(Object o);
	
}
