package demo.vikram.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import demo.vikram.domain.Movie;
import demo.vikram.exception.RecordNotFoundException;
import demo.vikram.service.MovieService;

@Controller
public class MovieController {

	private static final Logger logger = LoggerFactory.getLogger(MovieController.class);

	@Autowired
	private MovieService movieService;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
	
	@RequestMapping(value="/viewMovie.*" )
	public Movie findMovieById(@RequestParam(value="movieId")Integer movieId) throws RecordNotFoundException{
		
		logger.info("findMovieById (movieId={0}", movieId);
		
		Movie movie = movieService.findMovieById(movieId);
		return movie;		
	}
	
	@RequestMapping(value="/listAllMovies.*")
	public void listAllMovies(ModelMap modelMap){
		
		logger.info("listAllMovies");
		modelMap.addAttribute("listMovies", movieService.findAllMovies());
	}
	
	@RequestMapping(value="/saveMovie.*")
	public String saveOrUpdateMovie(Movie movie, ModelMap modelMap){
	
		logger.info("saveOrUpdateMovie {0}", movie);
		movieService.saveOrUpdateMovie(movie);
		modelMap.addAttribute("listMovies", movieService.findAllMovies());
		return "listAllMovies";
	}
	
	@RequestMapping(value="/newMovie.*")
	public ModelAndView loadAddMovieForm(){
		Movie movie = new Movie();
		ModelAndView modelAndView = new ModelAndView("newMovie");
		modelAndView.addObject(movie);
		return modelAndView;
	}
	
}

