package demo.vikram.controller;

import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import demo.vikram.domain.Movie;
import demo.vikram.domain.MovieList;
import demo.vikram.exception.RecordNotFoundException;
import demo.vikram.service.MovieService;

@Controller
@RequestMapping("/library/")
public class MovieRestController {

	private static final Logger logger = LoggerFactory.getLogger(MovieController.class);

	@Autowired
	private MovieService movieService;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
	
	@RequestMapping(value = "/movie/{id}", method = RequestMethod.GET)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public Movie findMovieById(@PathVariable("id") Integer movieId) throws RecordNotFoundException{
		
		logger.info("findMovieById {}", movieId);
		
		Movie movie = movieService.findMovieById(movieId);
		return movie;
	}
	
	@RequestMapping(value = "/movies", method= RequestMethod.GET)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public MovieList findAllMovies(){
		
		logger.info("findAllMovies");
		
		List<Movie> movies = movieService.findAllMovies();
		MovieList movieList = new MovieList();
		movieList.setMovie(movies);
		return movieList;
	}
	
	@RequestMapping(value = "/addMovie", method = RequestMethod.POST, headers="Accept=application/xml")
	public void addMovie(@RequestBody Movie movie) throws ParseException{
		
		logger.info("AddMovie, movieName={}", movie.getName());
		String dateRelease = movie.getRelease();
		dateRelease = dateRelease.replace("UTC ", "");
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd hh:mm:ss yyyy");
		Date dateOfRelease = sdf.parse(dateRelease);
		movie.setYearOfRelease(dateOfRelease);
		movieService.saveOrUpdateMovie(movie);
	}
	
	@ExceptionHandler(RecordNotFoundException.class)
	public void handleException(HttpServletResponse response, Writer writer) throws IOException{
		
		logger.info("Handling Exception");
		response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "No records exist");
	}
}
