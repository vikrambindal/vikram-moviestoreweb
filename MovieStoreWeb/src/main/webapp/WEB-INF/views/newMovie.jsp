<%@ include file="/WEB-INF/header.jsp" %>
<h1>Movie: Add New Movie</h1>

<form:form method="POST" action="saveMovie.htm" commandName="movie">
    <table border="1" cellspacing="5">
        <tbody>
            <tr>
                <td>Name</td>
                <td><form:input path="name"/></td>
            </tr>
            <tr>
                <td>Rating</td>
                <td><form:input path="rating"/></td>
            </tr>
            <tr>
                <td>Release</td>
                <td><form:input path="yearOfRelease"/></td>
            </tr>
            <tr>
                <td>Genre</td>
                <td><form:input path="genre"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Submit"/></td>
            </tr>
        </tbody>
    </table>

</form:form>

<%@ include file="/WEB-INF/footer.jsp" %>
