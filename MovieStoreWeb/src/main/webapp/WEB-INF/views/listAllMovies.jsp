<%@ include file="/WEB-INF/header.jsp" %>
<h1>List of Movies</h1>

<a href="./newMovie.htm">Add Movie</a>

<c:choose>
    <c:when test="${listMovies == null}">
        <p>No movies currently exist</p>
    </c:when>
    <c:otherwise>

        <table border="0" cellspacing="10">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Rating</th>
                    <th>Release</th>
                    <th>Genre</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${listMovies}" var="movie">
                    <tr>
                        <td>${movie.id}</td>
                        <td>
                            <a href="./viewMovie.htm?movieId=${movie.id}">
                                <c:out value="${movie.name}"/>
                            </a>
                        </td>
                        <td><c:out value="${movie.rating}"/></td>
                        <td><c:out value="${movie.yearOfRelease}"/></td>
                        <td><c:out value="${movie.genre}"/></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </c:otherwise>
</c:choose>

<%@ include file="/WEB-INF/footer.jsp" %>
